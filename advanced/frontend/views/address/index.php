﻿<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<div class="center_box cb">
    <div class="uo_tabs cf">
        <a href="#"><span>profile</a>
        <a href="#"><span>Reviews</span></a>
        <a href="#"><span>orders</span></a>
        <a href="#" class="active"><span>My Address</span></a>
        <a href="#"><span>Settings</span></a>
    </div>
    <div class="page_content bg_gray">
        <div class="uo_header">
            <div class="wrapper cf">
                <div class="wbox ava">
                    <figure><img src="imgc/user_ava_1_140.jpg" alt="Helena Afrassiabi"/></figure>
                </div>
                <div class="main_info">
                    <h1>Helena Afrassiabi</h1>
                    <div class="midbox">
                        <h4>560 points</h4>
                        <div class="info_nav">
                            <a href="#">Get Free Points</a>
                            <span class="sepor"></span>
                            <a href="#">Win iPad</a>
                        </div>
                    </div>
                    <div class="stat">
                        <div class="item">
                            <div class="num">30</div>
                            <div class="title">total orders</div>
                        </div>
                        <div class="item">
                            <div class="num">14</div>
                            <div class="title">total reviews</div>
                        </div>
                        <div class="item">
                            <div class="num">0</div>
                            <div class="title">total gifts</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uo_body">
            <div class="wrapper">
                <div class="uofb cf">
                    <div class="l_col adrs">
                        <h2>Add New Address</h2>

                        <?php echo $form = Html::beginForm(['address/save', 'class' => 'address_form'], 'post', ['enctype' => 'multipart/form-data']) ?>
                        <div class="field">
                            <label>Name *</label>
                            <?php echo Html::activeInput('text', $model, 'name', ['maxlength' => '100', 'class' => 'vl_empty']); ?>
                        </div>
                        <div class="field">
                            <label>Your city *</label>
                            <?php
                            echo Html::activeDropDownList($model, 'city_id', ArrayHelper::map($cities, 'id', 'name'), [
                                'prompt' => '',
                                'class' => 'vl_empty'
                            ]); ?>
                        </div>
                        <div class="field">
                            <label>Your area *</label>
                            <?php
                            echo Html::activeDropDownList($model, 'area_id', ArrayHelper::map($areas, 'id', 'name'), [
                                'prompt' => '',
                                'class' => 'vl_empty'
                            ]); ?>
                        </div>

                        <div class="field">
                            <label>Street</label>
                            <?php echo Html::activeInput('text', $model, 'street', ['maxlength' => '200', 'class' => 'vl_empty']); ?>
                        </div>
                        <div class="field">
                            <label>House # </label>
                            <?php echo Html::activeInput('text', $model, 'house', ['maxlength' => '200', 'class' => 'vl_empty', 'placeholder' => 'House Name / Number']); ?>
                        </div>

                        <div class="field">
                            <label class="pos_top">Additional information</label>
                            <?php echo Html::activeTextarea($model, 'house'); ?>
                        </div>

                        <div class="field">
                            <?php echo Html::submitButton(Yii::t('app', 'add address'), ['value' => 'add address', 'class' => 'green_btn']) ?>
                        </div>
                        <?php echo Html::endForm(); ?>
                    </div>
                    <div class="r_col">
                        <h2>My Addresses</h2>
                        <div class="uo_adr_list">
                            <?php

                            foreach ($addresses as $address) {
                                $city = !empty($address->city->name) ? $address->city->name . ', ' : '';
                                $area = !empty($address->area->name) ? $address->area->name . ', ' : '';
                                $additionalInfo = isset($address->additional) ? '<br/>' . $address->additional : '';
                                ?>
                                <div class="item">
                                    <h3><?= $address['name'] ?></h3>
                                    <p><?= $city . $area . $address->street . ', ' . $address->house . $additionalInfo; ?></p>
                                    <div class="actbox">
                                        <a href="#" class="bcross"></a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




