<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;

use frontend\models\Address;
use frontend\models\City;
use frontend\models\Area;
use yii\web\Response;


/**
 * Site controller
 */
class AddressController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $userId = 1;
        //TODO выборка объектов.
        $addresses = Address::find(['user_id' => $userId])
            ->with('area', 'city')
            ->all();
        return $this->render('index', [
            'model' => new Address,
            'addresses' => $addresses,
            'cities' => City::find()->all(),
            'areas' => Area::find()->all()
        ]);
    }

    public function actionSave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Address;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'status' => 200
            ];
        } else {
            return [
                'status' => 400
            ];
        }
    }
}
